import _imports
import pytest
import pybi.utils.sql as sqlUtils


def test_extract_table_names_normal():

    act = sqlUtils.extract_table_names("select 类别,名字,值 from test")

    exp = ["test"]

    assert act == exp


def test_extract_table_names_normal_upper():

    act = sqlUtils.extract_table_names("SELECT 类别,名字,值 FROM test")

    exp = ["test"]

    assert act == exp


def test_extract_table_names_with_where():

    act = sqlUtils.extract_table_names("select a from test where a=='from'")

    exp = ["test"]

    assert act == exp


def test_extract_table_names_with_query():

    act = sqlUtils.extract_table_names("select a (select * from test) where a=='from'")

    exp = ["test"]

    assert act == exp


def test_extract_table_names_with_join():

    act = sqlUtils.extract_table_names(
        "select a from tab1 as t1 join tab2 as t2 on t1.key=t2.key  where t1.a=='from'"
    )

    exp = ["tab1", "tab2"]

    assert act == exp


def test_extract_fields_head_select():
    act = sqlUtils.extract_fields_head_select("select a,b,c from (select x from tab1)")

    exp = ["a", "b", "c"]

    assert act == exp


def test_extract_fields_head_select_alias():
    act = sqlUtils.extract_fields_head_select(
        "select a as name1,b,c from (select x from tab1)"
    )

    exp = ["name1", "b", "c"]

    assert act == exp


def test_extract_fields_head_select_func():
    act = sqlUtils.extract_fields_head_select(
        "select a, round(avg(b),2) as fun1,round(avg(c),2) as fun2  from tab"
    )

    exp = ["a", "fun1", "fun2"]

    assert act == exp


def test_extract_fields_head_select_mul_lines():
    act = sqlUtils.extract_fields_head_select(
        """select a, 
        round(avg(b),2) as fun1,
        round(avg(c),2) as fun2  
        from tab"""
    )

    exp = ["a", "fun1", "fun2"]

    assert act == exp


def test_extract_table_names_apply_agg():
    agg = "round(avg(${}),2)"
    y = "销量"

    act = sqlUtils.apply_agg(agg, y)
    exp = "round(avg(销量),2)"

    assert exp == act


def test_extract_table_names_univ_apply_agg():
    agg = "sum"
    y = "销量"

    act = sqlUtils.apply_agg(agg, y)
    exp = f"{agg}(`{y}`)"

    assert exp == act
