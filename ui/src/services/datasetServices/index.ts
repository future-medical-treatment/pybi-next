import { TDataSource, TDataView, TDataViewBase, TPivotDataView } from "@/models/dataSources";
import { create as createDataSource } from "./dataSource";
import { create as createDataView, createPivotDataView } from "./dataView";
import { IDataset, IDataView, IPivotDataView } from "./types";
import { TSqlAnalyzeService } from "@/services/sqlAnalyzeServices";
import { App } from "@/models/app";
import { Ref } from "vue";
import { TCpId } from "@/models/types";
import { TComponentServices, iterComponent } from "@/services/componentServices";
import { TDbServices } from "@/services/dbServices";


export type TDatasetServices = ReturnType<typeof getServices>

export function getServices(app: App, services: {
    sqlAnalyze: TSqlAnalyzeService,
    component: TComponentServices,
    db: TDbServices,
}) {

    const datasetRel = createDataSetRelationship(app.dataSources, app.dataViews, services)

    registerFilters(app, datasetRel)

    function createSql(datasetName: string, requestorId: string): Ref<string> {
        const ds = datasetRel.getDataset(datasetName)
        return ds.toSqlWithFilters(requestorId)
    }

    function addFilter(cpid: TCpId, datasetName: string, expression: string) {
        datasetRel.getDataset(datasetName).addFilter(cpid, expression)
    }

    function removeFilters(cpid: TCpId, datasetName: string) {
        datasetRel.getDataset(datasetName).removeFilters(cpid)
    }



    return {
        createSql,
        addFilter,
        removeFilters,
        getDataset: datasetRel.getDataset,
    }

}


function createDataSetRelationship(dataSources: TDataSource[], dataViews: TDataViewBase[], services: {
    sqlAnalyze: TSqlAnalyzeService,
    component: TComponentServices,
    db: TDbServices,
}) {

    const dataSetMap = new Map<string, IDataset>()

    function getFromMapMaybeError(name: string) {
        const ds = dataSetMap.get(name)
        if (!ds) {
            throw new Error(`not found dataset[name:${name}] in mapping`);
        }
        return ds
    }

    dataSources.forEach(ds => {
        const dsObj = createDataSource(ds.name, { component: services.component })
        dataSetMap.set(ds.name, dsObj)
    })

    dataViews.forEach(dv => {

        switch (dv.type) {
            case 'sql':
                {
                    const sqlDv = dv as TDataView
                    const dvObj = createDataView(sqlDv.name, sqlDv.sql, sqlDv.excludeLinkages, { component: services.component })
                    dataSetMap.set(dv.name, dvObj)
                }
                break;

            case 'pivot':
                {
                    const sqlDv = dv as TPivotDataView
                    const dvObj = createPivotDataView(sqlDv.name, sqlDv.source, sqlDv.pivotOptions, sqlDv.excludeLinkages, services)
                    dataSetMap.set(dv.name, dvObj)
                }
                break;
            default:
                break;
        }

    })


    for (const ds of dataSetMap.values()) {
        if (ds.typeName === 'dataView') {
            const dv = ds as IDataView
            const tabs = services.sqlAnalyze.getTableNames(dv.sql)

            tabs.forEach(tab => {

                const ds = getFromMapMaybeError(tab)
                dv.addLinkageDataset(ds)
            })

        }

        if (ds.typeName === 'pivot-dataView') {
            const dv = ds as IPivotDataView
            const sourceDs = getFromMapMaybeError(dv.sourceDatasetName)
            dv.addLinkageDataset(sourceDs)
        }

    }

    function getDataset(name: string) {
        const ds = getFromMapMaybeError(name)
        return ds
    }


    return {
        getDataset,
    }
}


type TUpdateInfo = {
    table: string
    field: string
}

type TDatasetRel = ReturnType<typeof createDataSetRelationship>

function registerFilters(app: App, datasetRel: TDatasetRel) {

    for (const cp of iterComponent(app)) {
        if ('updateInfos' in cp) {
            (cp.updateInfos as TUpdateInfo[]).forEach(info => {
                const ds = datasetRel.getDataset(info.table)
                ds.initFilter(cp.id)
            })
        }
    }

}