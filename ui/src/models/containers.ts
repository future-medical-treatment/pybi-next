import { Component } from "./component"


export interface Container extends Component {
    children: Component[]
}


export interface BoxContainer extends Container {
}

export interface GridContainer extends Container {
    areas: string
    gridTemplateColumns: string
    gridTemplateRows: string
}


export interface ColBoxContainer extends Container {
    spec: number[]
}

export interface FlowBoxContainer extends Container {

}

export interface TabsContainer extends Container {
    names: string[]
    mode: 'fullWidth' | 'narrowing'
}