


export enum StatementType {
    Init = "Init",
    DataSource = "DataSource",
    Component = "Component",
    For = "For",
    If = "If",
}


export interface Statement {
    id: string
    type: StatementType
    children: Statement[]
}

export interface IfStatement extends Statement {
    linkages: string[]
    // sourceName: string | null
    sqlInfo: {
        sql: string
        mappings: string[]
    }
}